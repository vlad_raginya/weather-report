var date = new Date();

$('.date').text(date.toDateString());

function selectImages(data){
    switch(data.weather[0].main){
        case "Clear":
       
         if(date.getHours() >= 20 || date.getHours() < 6){
           $("body").css("background-image", "url('images/clear_night.jpg')");
           $(".weather_image").attr("src", "images/clear_night_sym.png");
           
        }
         else {
          $("body").css("background-image", "url('images/sunny.jpg')");
          $(".weather_image").attr("src", "images/clear_day_sym.png");
         }
         break;
        case "Rain": case "Drizzle":
        $("body").css("background-image", "url('images/rainy.jpg')");
        $(".weather_image").attr("src", "images/rain_sym.png");
        break;
        case "Clouds":
        $("body").css("background-image", "url('images/cloudy.jpg')");
        $(".weather_image").attr("src", "images/cloudy_sym.png");
        break;
        case "Thunderstorm":
        $("body").css("background-image", "url('images/thunderstorm.jpg')");
        $(".weather_image").attr("src", "images/thunderstorm_sym.png");
        break;
        case "Fog": case "Mist": case "Haze":
        $("body").css("background-image", "url('images/fog_mist_haze.jpg')");
        $(".weather_image").attr("src", "images/fog_mist_haze_sym.png");
        break;
        case "Snow":
        $("body").css("background-image", "url('images/snowy.jpg')");
        $(".weather_image").attr("src", "images/snowy_sym.png");
        break;
    }
    
}

function errorMessage(data)
{
  $(".error_container").text("This city is not found. Try again");
  $(".error_container").slideDown(300);
}

function drawWeather( data ) {

    $('.cityname').text(data.name + ", " + data.sys.country);
    $('.weather').text(data.weather[0].main);
    selectImages(data);
    $('.temperature').text("Temperature: " + Math.round((parseFloat(data.main.temp) - 273.15))+ "°C");
    $('.windspeed').text("Wind Speed: " + data.wind.speed + "m/s");
    $('.humidity').text("Humidity: " + data.main.humidity + "%");
    $('.pressure').text("Pressure: " + data.main.pressure + "hpa");
    
}

function getWeatherData( cityName ) {
    var key = '3eb70ce0c0ff78c06b934409a65e40ed';
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + cityName+ '&appid=' + key)  
    .then(function(resp) { return resp.json() }) // Convert data to json
    .then(function(data) {
        console.log(data);
        drawWeather(data);
        $(".weather_container").show({duration: 300, start: function () {

          $(".error_container").slideUp(300);
          $(".weather_container").css("display","flex");

        }});
    })
    .catch(function(err) {
      errorMessage(err);
    });
  }
  
  $("#search_btn").click( function() {

    getWeatherData($("#search_input").val());
    $(".weather_container").hide(300);
  });
  
  $("#search_input").keypress(function() {
    
    if(event.which == 13) {
     getWeatherData($("#search_input").val());
     $(".weather_container").hide(300);
    }

  });
